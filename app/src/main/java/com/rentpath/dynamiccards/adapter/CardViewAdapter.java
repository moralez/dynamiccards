package com.rentpath.dynamiccards.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.rentpath.dynamiccards.CardTouchHelper;
import com.rentpath.dynamiccards.model.Card;
import com.rentpath.dynamiccards.viewholder.CardViewHolder;

import java.util.List;

/**
 * Created by jmo on 4/1/2016.
 */
public class CardViewAdapter extends RecyclerView.Adapter<CardViewHolder>
        implements CardTouchHelper.CardTouchListener {

    private List<Card> mDataset;

    public CardViewAdapter(List<Card> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public int getItemViewType(int position) {
        return mDataset.get(position).getCardViewType().ordinal();
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return CardViewHolder.getViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        holder.setCardData(mDataset.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDismiss(int position) {
        mDataset.remove(position);
        notifyItemRemoved(position);
    }
}
