package com.rentpath.dynamiccards.model;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by jmoralez on 4/21/16.
 */
public class ContentRowSegment implements ViewProvider {

    private String mContent;
    private String mContentDrawable;
    private float mWeight;
    private int mAlignment;
    private Context mContext;

    public ContentRowSegment(Context context, String content, float weight, int alignment) {
        this(context, content, null, weight, alignment);
    }

    public ContentRowSegment(Context context, String text, String compoundDrawable, float weight, int alignment) {
        this.mContent = text;
        this.mContentDrawable = compoundDrawable;
        this.mWeight = weight;
        this.mAlignment = alignment;
        this.mContext = context;
    }

    public View getView(Context context) {
        View mView;
        if (mContent.contains(".png")) {
            ImageView imageView = new ImageView(mContext);
            Resources resources = mContext.getResources();
            int resourceId = resources.getIdentifier(mContent.replace(".png", ""), "drawable", mContext.getPackageName());
            imageView.setImageDrawable(ResourcesCompat.getDrawable(resources, resourceId, null));

            mView = imageView;
        } else {
            TextView textView = new TextView(mContext);
            textView.setText(mContent);
            textView.setGravity(mAlignment);

            if (null != mContentDrawable) {
                Resources resources = mContext.getResources();
                int resourceId = resources.getIdentifier(mContentDrawable.replace(".png", ""), "drawable", mContext.getPackageName());
                Drawable drawable = ResourcesCompat.getDrawable(resources, resourceId, null);
                drawable.setBounds(0, 0, 100, 100);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    textView.setCompoundDrawablesRelative(drawable, null, null, null);
                } else {
                    textView.setCompoundDrawables(drawable, null, null, null);
                }
            }

            mView = textView;
        }
        LinearLayout.LayoutParams param;
        if (mWeight == 0) {
            param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            param = new LinearLayout.LayoutParams(100,100);
        } else {
            param = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
            param.weight = mWeight;
        }
        param.gravity = mAlignment;
        mView.setLayoutParams(param);
        mView.setPadding(10, 10, 10, 10);
        return mView;
    }

    public float getWeight() {
        return mWeight;
    }

    public int getAlignment() {
        return mAlignment;
    }
}
