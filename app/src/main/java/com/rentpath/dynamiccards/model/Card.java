package com.rentpath.dynamiccards.model;

import android.support.v4.app.FragmentManager;

import com.google.gson.annotations.SerializedName;
import com.rentpath.dynamiccards.view.ContentRowView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmo on 4/1/2016.
 */
public class Card {

    private CardViewType mCardViewType;

    private String mTitle;
    private String mContent;
    private String mSubcontent;

    private List<String> mImageUrls;
    private FragmentManager mFragmentManager;

    private List<ContentRowView> mContentViews;

    public Card(CardViewType cardViewType, String title, String content) {
        this(cardViewType, title, content, null);
    }

    public Card(CardViewType cardViewType, String title, String content, String subcontent) {
        mCardViewType = cardViewType;
        mTitle = title;
        mContent = content;
        mSubcontent = subcontent;
    }

    public Card(CardViewType cardViewType, FragmentManager fragmentManager, List<String> imageUrls,
                List<ContentRowView> contentRowViews) {
        mCardViewType = cardViewType;
        mFragmentManager = fragmentManager;
        mImageUrls = imageUrls;
        mContentViews = contentRowViews;
    }

     public Card(CardViewType cardViewType, String title, List<ContentRowView> contentDetails) {
         mCardViewType = cardViewType;
         mTitle = title;
         if (contentDetails == null) {
             mContentViews = new ArrayList<>();
         } else {
             mContentViews = contentDetails;
         }
     }

    public CardViewType getCardViewType() {
        return mCardViewType;
    }

    public String getTitle() {
        return mTitle == null ? "" : mTitle;
    }

    public String getContent() {
        return mContent;
    }

    public String getSubcontent() {
        return mSubcontent;
    }

    public List<String> getImageUrls() {
        return mImageUrls;
    }

    public FragmentManager getFragmentManager() {
        return mFragmentManager;
    }

    public List<ContentRowView> getContentViews() {
        return mContentViews;
    }
}
