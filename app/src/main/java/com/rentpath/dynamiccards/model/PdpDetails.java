package com.rentpath.dynamiccards.model;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * This is the class used to parse the JSON through RetroFit
 *
 * Created by jmo on 4/27/2016.
 */
public class PdpDetails {
    @SerializedName("title-view")
    public TitleView titleView;
    @SerializedName("action-view")
    public ActionView actionView;
    public List<Card> cards;

    private class TitleView {
        public String type;
        public String title;
        public String subtitle;
    }

    private class ActionView {
        public int height;
        @SerializedName("background-color")
        public String backgroundColor;
        @SerializedName("divider-color")
        public String dividerColor;
        @SerializedName("show-dividers")
        public boolean showDividers;
        public List<ActionDetails> actions;
    }

    private class ActionDetails {
        public String text;
        public String color;
        @SerializedName("font-size")
        public int fontSize;
        @SerializedName("background-color")
        public String backgroundColor;
        public Action action;
    }

    private class Action {
        public String type;
        public ActionData data;
        @SerializedName("target-card-id")
        public String targetCardId;
    }

    private class ActionData {
        @SerializedName("view-controller-type")
        public String viewType;
        @SerializedName("close-button-title")
        public String closeButtonTitle;
    }

    public class Card {
        public String type;
        public List<Content> contents;

        public CardViewType getCardViewType() {
            CardViewType cardViewType = CardViewType.EXPANDABLE;
            if (type.equals("gallery-price-card")) {
                cardViewType = CardViewType.VIEW_PAGER;
            } else if (type.equals("description-card")) {
                cardViewType = CardViewType.DYNAMIC;
            } else if (type.equals("floorplans-card")
                    || type.equals("amenities-card")
                    || type.equals("office-hours-card")
                    || type.equals("ratings-reviews-card")
                    || type.equals("share-card")) {
                cardViewType = CardViewType.DYNAMIC;
            }
            return cardViewType;
        }
    }

    public class Content {
        public String type;
        public Data data;

        public List<String> getPhotos() {
            return data.photos;
        }

        public List<DataView> getViews() {
            return data.views;
        }
    }

    public class Data implements ViewProvider {
        public List<String> photos;
        public int grid;
        public int height;
        public List<DataView> views;
        public String id;
        public String text;
        @SerializedName("margin-left")
        public int marginLeft;
        @SerializedName("margin-top")
        public int marginTop;
        @SerializedName("margin-right")
        public int marginRight;
        @SerializedName("margin-bottom")
        public int marginBottom;

        public View getView(Context context) {
            View mView;
            TextView textView = new TextView(context);
            textView.setText(text);
            mView = textView;

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            mView.setLayoutParams(param);
            mView.setPadding(10, 10, 10, 10);

            return mView;
        }
    }

    public class DataView implements ViewProvider {
        public String type;
        public List<DataViewIamge> images;
        public String text;
        @SerializedName("scale-font")
        public boolean scaleFont;
        @SerializedName("font-size")
        public int fontSize;
        public String color;
        public String alignment;
        @SerializedName("content-hugging")
        public int contentHugging;
        @SerializedName("margin-left")
        public int marginLeft;
        @SerializedName("margin-right")
        public int marginRight;
        public int weight;

        public View getView(Context context) {
            View mView;
            if (type.equals("images")) {
                LinearLayout imageViews = new LinearLayout(context);
                imageViews.setOrientation(LinearLayout.HORIZONTAL);
                for (DataViewIamge image : images) {
                    imageViews.addView(image.getView(context));
                }
                mView = imageViews;
            } else if (type.equals("text-label")) {
                TextView textView = new TextView(context);
                textView.setText(text);
                // scale font ?
                textView.setTextSize(fontSize);
                if (null != color) {
                    try {
                        textView.setTextColor(Color.parseColor(color));
                    } catch (IllegalArgumentException e) {
                        Log.e("JMO", "ERROR: " + e.getLocalizedMessage());
                    }
                }
                textView.setGravity(getGravityFromAlignment());
                // content hugging ?
                mView = textView;
            } else if (type.equals("text")){
                TextView textView = new TextView(context);
                textView.setText(text);
                // scale font ?
                textView.setTextSize(fontSize);
                textView.setGravity(getGravityFromAlignment());
                // content hugging ?
                mView = textView;
            } else {
                TextView textView = new TextView(context);
                textView.setText(text);
                // scale font ?
                textView.setTextSize(fontSize);
                textView.setGravity(getGravityFromAlignment());
                // content hugging ?
                mView = textView;
            }

            LinearLayout.LayoutParams param;
            if (0 == weight) {
                param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
            } else {
                param = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
                param.weight = weight;
            }
            param.gravity = getGravityFromAlignment();

            mView.setLayoutParams(param);
            mView.setPadding(10, 10, 10, 10);

            return mView;
        }

        private int getGravityFromAlignment() {
            if (null != alignment&& alignment.equals("center")) {
                return Gravity.CENTER;
            }
            return Gravity.NO_GRAVITY;
        }
    }

    private class DataViewIamge {
        public String url; // can also be name of image
        public int width;
        @SerializedName("margin-left")
        public int marginLeft;

        public ImageView getView(Context context) {
            Resources resources = context.getResources();
            ImageView imageView = new ImageView(context);
            int resourceId = resources.getIdentifier(url.replace(".png", "").replace("-", "_"), "drawable", context.getPackageName());
            try {
                imageView.setImageDrawable(ResourcesCompat.getDrawable(resources, resourceId, null));
                LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                imageParams.setMargins(marginLeft, 0, 0, 0);
                imageView.setLayoutParams(imageParams);
            } catch (Resources.NotFoundException e) {
                Log.e("JMO", "No image resource by the name " + url + " found: " + e.getLocalizedMessage());
            }
            return imageView;
        }
    }
}
