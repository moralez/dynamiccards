package com.rentpath.dynamiccards.model;

/**
 * Created by jmo on 4/1/2016.
 */
public enum CardViewType {
    EXPANDABLE,
    VIEW_PAGER,
    DYNAMIC;

    private static final CardViewType[] values = CardViewType.values();
    public static CardViewType getCardViewType(int viewType) {
        return (viewType < 0 || viewType > values.length) ? null : values[viewType];
    }
}