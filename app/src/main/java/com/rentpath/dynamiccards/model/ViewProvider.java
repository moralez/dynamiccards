package com.rentpath.dynamiccards.model;

import android.content.Context;
import android.view.View;

/**
 * Created by jmo on 4/29/2016.
 */
public interface ViewProvider {
    View getView(Context context);
}
