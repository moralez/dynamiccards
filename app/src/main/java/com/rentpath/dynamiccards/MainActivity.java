package com.rentpath.dynamiccards;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.rentpath.dynamiccards.adapter.CardViewAdapter;
import com.rentpath.dynamiccards.model.Card;
import com.rentpath.dynamiccards.model.CardViewType;
import com.rentpath.dynamiccards.model.ContentRowSegment;
import com.rentpath.dynamiccards.model.PdpDetails;
import com.rentpath.dynamiccards.model.ViewProvider;
import com.rentpath.dynamiccards.service.PdpDataService;
import com.rentpath.dynamiccards.view.ContentRowView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    ArrayList<Card> cards = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://hitme.jeremyfox.me/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PdpDataService service = retrofit.create(PdpDataService.class);
        Call<PdpDetails> detailsCall = service.getPdpDetails();

        detailsCall.enqueue(new Callback<PdpDetails>() {
            @Override
            public void onResponse(Call<PdpDetails> call, Response<PdpDetails> response) {
                PdpDetails details = response.body();

                ArrayList<Card> responseCards = new ArrayList<>();
                for (PdpDetails.Card card : details.cards) {
                    switch (card.getCardViewType()) {
                        case VIEW_PAGER: {
                            List<PdpDetails.Content> cardContents = card.contents;
                            ArrayList<String> photoUrls = new ArrayList<String>();
                            ArrayList<ContentRowView> contentRowViews = new ArrayList<>();

                            for (PdpDetails.Content content : cardContents) {
                                if (content.type.equals("separator")) {
                                    // If needed add a ContentRowView here for the separator
                                } else if (content.type.equals("gallery")) {
                                    photoUrls = (ArrayList) content.getPhotos();
                                } else if (content.type.equals("flex")) {
                                    ContentRowView contentRowView = new ContentRowView(MainActivity.this, content.getViews(), 0);
                                    contentRowViews.add(contentRowView);
                                }
                            }
                            Card viewPagerCard = new Card(card.getCardViewType(), getSupportFragmentManager(), photoUrls, contentRowViews);
                            responseCards.add(viewPagerCard);
                            break;
                        }
                        case DYNAMIC: {
                            List<PdpDetails.Content> cardContents = card.contents;
                            ArrayList<ContentRowView> contentRowViews = new ArrayList<>();

                            for (PdpDetails.Content content : cardContents) {
                                if (content.type.equals("separator")) {
                                    // If needed add a ContentRowView here for the separator
                                } else if (content.type.equals("flex")) {
                                    ContentRowView contentRowView = new ContentRowView(MainActivity.this, content.getViews(), 0);
                                    contentRowViews.add(contentRowView);
                                } else if (content.type.equals("text")) {
                                    ContentRowView contentRowView = new ContentRowView(MainActivity.this, Arrays.asList(content.data), 0);
                                    contentRowViews.add(contentRowView);
                                }
                            }
                            Card expandableCard = new Card(card.getCardViewType(), card.type, contentRowViews);
                            responseCards.add(expandableCard);
                            break;
                        }
                        default:
                            break;
                    }
                }

                cards.addAll(responseCards);
                mAdapter.notifyDataSetChanged();
//                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<PdpDetails> call, Throwable t) {
                Log.i("JMO", "eeeeeeeek " + t.getLocalizedMessage());
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        mRecyclerView .addItemDecoration(new VerticalSpaceItemDecoration(48));

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new CardViewAdapter(cards);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
