package com.rentpath.dynamiccards.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rentpath.dynamiccards.R;
import com.rentpath.dynamiccards.model.Card;
import com.rentpath.dynamiccards.model.CardViewType;

/**
 * This is the default class for loading card views through the recycler view
 * You can add new types of views and instantiate them here so they are drawn
 *
 * Created by jmo on 4/1/2016.
 */
public abstract class CardViewHolder extends RecyclerView.ViewHolder {
    public CardViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void setCardData(Card card);

    public static CardViewHolder getViewHolder(ViewGroup parent, int viewType) {
        View v;
        CardViewHolder vh;
        CardViewType cardViewType = CardViewType.getCardViewType(viewType);
        Context context = parent.getContext();
        switch (cardViewType) {
            case EXPANDABLE:
                v = LayoutInflater.from(context).inflate(R.layout.layout_expandable_card, parent, false);
                vh = new ExpandableCardViewHolder(v);
                break;
            case VIEW_PAGER:
                v = LayoutInflater.from(context).inflate(R.layout.layout_viewpager_card, parent, false);
                vh = new ViewPagerCardViewHolder(v);
                break;
            case DYNAMIC:
                v = LayoutInflater.from(context).inflate(R.layout.layout_dynamic_card, parent, false);
                vh = new DynamicCardViewHolder(v);
                break;
            default:
                v = LayoutInflater.from(context).inflate(R.layout.layout_empty_card, parent, false);
                vh = new EmptyCardViewHolder(v);
                break;
        }
        return vh;
    }
}
