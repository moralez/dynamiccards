package com.rentpath.dynamiccards.viewholder;

import android.view.View;
import android.widget.TextView;

import com.rentpath.dynamiccards.R;
import com.rentpath.dynamiccards.model.Card;
import com.rentpath.dynamiccards.view.ExpandableTextView;

/**
 * Created by jmo on 4/1/2016.
 */
public class ExpandableCardViewHolder extends CardViewHolder {

    private TextView mTitleTextView;
    private ExpandableTextView mContentTextView;

    public ExpandableCardViewHolder(View itemView) {
        super(itemView);
        mTitleTextView = (TextView)itemView.findViewById(R.id.card_title);
        mContentTextView = (ExpandableTextView)itemView.findViewById(R.id.card_content);
    }

    @Override
    public void setCardData(Card card) {
        String title = card.getTitle();
        if (title.length() > 0) {
            mTitleTextView.setVisibility(View.VISIBLE);
            mTitleTextView.setText(title);
        } else {
            mTitleTextView.setVisibility(View.GONE);
        }

        String content = card.getContent();
        if (null != content && content.length() > 0) {
            mContentTextView.setVisibility(View.VISIBLE);
            mContentTextView.setContentText(content);
        } else {
            mContentTextView.setVisibility(View.GONE);
        }
    }
}
