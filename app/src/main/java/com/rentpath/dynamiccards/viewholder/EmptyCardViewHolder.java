package com.rentpath.dynamiccards.viewholder;

import android.view.View;

import com.rentpath.dynamiccards.model.Card;

/**
 * Created by jmo on 4/1/2016.
 */
public class EmptyCardViewHolder extends CardViewHolder {

    public EmptyCardViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setCardData(Card card) {

    }
}
