package com.rentpath.dynamiccards.viewholder;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

import com.rentpath.dynamiccards.R;
import com.rentpath.dynamiccards.fragment.ImagePageFragment;
import com.rentpath.dynamiccards.model.Card;
import com.rentpath.dynamiccards.view.ContentRowView;

import java.util.List;

/**
 * Created by jmo on 4/1/2016.
 */
public class ViewPagerCardViewHolder extends CardViewHolder {

    private ViewPager mViewPager;
    private LinearLayout mContentView;

    public ViewPagerCardViewHolder(View itemView) {
        super(itemView);
        mViewPager = (ViewPager)itemView.findViewById(R.id.pager);
        mContentView = (LinearLayout)itemView.findViewById(R.id.dynamic_content);
    }

    @Override
    public void setCardData(Card card) {
        mViewPager.setAdapter(new ImagePagerAdapter(card.getFragmentManager(), card.getImageUrls()));
        mContentView.removeAllViews();
        for (ContentRowView contentRowView : card.getContentViews()) {
            if (null == contentRowView.getView().getParent()) {
                mContentView.addView(contentRowView.getView());
            }
        }
    }

    private class ImagePagerAdapter extends FragmentStatePagerAdapter {

        private List<String> mContent;

        private ImagePagerAdapter(FragmentManager fm, List<String> content) {
            super(fm);
            mContent = content;
        }

        @Override
        public int getCount() {
            return mContent.size();
        }

        @Override
        public Fragment getItem(int position) {
            ImagePageFragment page = new ImagePageFragment();
            Bundle pageArgs = new Bundle();
            pageArgs.putString("imageUrl", mContent.get(position));
            page.setArguments(pageArgs);
            return page;
        }
    }
}
