package com.rentpath.dynamiccards.viewholder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rentpath.dynamiccards.R;
import com.rentpath.dynamiccards.model.Card;
import com.rentpath.dynamiccards.view.ExpandableLinearLayout;

/**
 * Created by jmoralez on 4/18/16.
 */
public class DynamicCardViewHolder extends CardViewHolder {

    private LinearLayout mContentViewsLayout;
    private TextView mTitleTextView;

    private ExpandableLinearLayout mDynamicContent;

    public DynamicCardViewHolder(View itemView) {
        super(itemView);
        mContentViewsLayout = (LinearLayout)itemView.findViewById(R.id.dynamic_content);
        mTitleTextView = (TextView)itemView.findViewById(R.id.title);
        mDynamicContent = (ExpandableLinearLayout)itemView.findViewById(R.id.expandable_dynamic_content);
    }

    @Override
    public void setCardData(Card card) {
        mTitleTextView.setText(card.getTitle());
        mDynamicContent.setContentViews(card.getContentViews());
    }
}
