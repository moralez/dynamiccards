package com.rentpath.dynamiccards.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rentpath.dynamiccards.R;

import java.util.List;

/**
 * Created by jmoralez on 4/21/16.
 */
public class ExpandableLinearLayout extends LinearLayout {

    private FrameLayout mContentContainer;
    private LinearLayout mContent;
    private TextView mAction;
    private float mMaxHeight;
    private final float mDefaultHeight = 700f;
    private int mMaxMeasuredContentLayoutHeight = -1;
    private boolean collapsed = true;

    private LinearLayout.LayoutParams maxHeightParams;
    private final LinearLayout.LayoutParams wrapContentParams =
            new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

    public ExpandableLinearLayout(Context context) {
        super(context);
    }

    public ExpandableLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_expandable_linear_layout, this, true);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.ExpandableLinearLayout, 0, 0);
        mMaxHeight = a.getDimension(R.styleable.ExpandableLinearLayout_maxHeight, mDefaultHeight);
        a.recycle();

        maxHeightParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int)mMaxHeight);

        mContentContainer = (FrameLayout) getChildAt(0);
        mContent = (LinearLayout)mContentContainer.getChildAt(0);

        mAction = (TextView) getChildAt(1);
        mAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                float height;
                if (collapsed) {
                    height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    mAction.setText("Show Less");
                } else {
                    height = mMaxHeight;
                    mAction.setText("Show More");
                }
                LinearLayout.LayoutParams params= new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int)height);
                mContentContainer.setLayoutParams(params);
                collapsed = !collapsed;
            }
        });
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        mMaxMeasuredContentLayoutHeight = Math.max(mMaxMeasuredContentLayoutHeight,
                mContent.getMeasuredHeight());

        if (mMaxMeasuredContentLayoutHeight < mMaxHeight) {
            mAction.setVisibility(View.GONE);
        } else {
            if (collapsed) {
                mContentContainer.setLayoutParams(maxHeightParams);
            } else {
                mContentContainer.setLayoutParams(wrapContentParams);
            }
        }
    }

    public void setContentViews(List<ContentRowView> contentViews) {
        mContent.removeAllViews();
        for (ContentRowView contentRowView : contentViews) {
            if (null == contentRowView.getView().getParent()) {
                mContent.addView(contentRowView.getView());
            }
        }
    }

    public void setActionText(String actionText) {
        mAction.setText(actionText);
    }
}
