package com.rentpath.dynamiccards.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rentpath.dynamiccards.R;

/**
 * Created by jmo on 4/19/2016.
 */
public class ExpandableTextView extends LinearLayout {

    private TextView mContent;
    private TextView mAction;
    private int mLineCount;

    private boolean actionToggled = false;

    public ExpandableTextView(Context context) {
        super(context);
    }

    public ExpandableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_expandable_text_view, this, true);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.ExpandableTextView, 0, 0);
        String contentText = a.getString(R.styleable.ExpandableTextView_contentText);
        String actionText = a.getString(R.styleable.ExpandableTextView_actionText);
        mLineCount = a.getInteger(R.styleable.ExpandableTextView_contentMaxLines, 5);
        a.recycle();

        mContent = (TextView) getChildAt(0);
        if (null != contentText && !contentText.isEmpty()) {
            mContent.setText(contentText);
        }
        mContent.setMaxLines(mLineCount);
        mContent.setEllipsize(TextUtils.TruncateAt.END);

        mAction = (TextView) getChildAt(1);
        if (null != actionText && !actionText.isEmpty()) {
            mAction.setText(actionText);
        }

        mAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                actionToggled = true;
                if (mContent.getEllipsize() == TextUtils.TruncateAt.END) {
                    mContent.setMaxLines(Integer.MAX_VALUE);
                    mContent.setEllipsize(null);
                } else {
                    mContent.setMaxLines(mLineCount);
                    mContent.setEllipsize(TextUtils.TruncateAt.END);
                }
            }
        });
    }

    public ExpandableTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        Layout contentLayout = mContent.getLayout();
        if (actionToggled) {
            if (contentLayout != null &&
                (contentLayout.getLineCount() == 0 ||
                contentLayout.getEllipsisCount(contentLayout.getLineCount()-1) == 0)) {
                mAction.setText("Show Less");
            } else {
                mAction.setText("Show More");
            }
        } else {
            if (contentLayout != null &&
                (contentLayout.getLineCount() == 0 ||
                contentLayout.getEllipsisCount(contentLayout.getLineCount() - 1) == 0)) {
                mAction.setVisibility(View.GONE);
            }
        }
    }

    public void setContentText(String contentText) {
        mContent.setText(contentText);
    }

    public void setActionText(String actionText) {
        mAction.setText(actionText);
    }
}
