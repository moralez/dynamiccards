package com.rentpath.dynamiccards.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by jmo on 4/4/2016.
 */
public class ContentDetailsHeaderView extends LinearLayout {
    public ContentDetailsHeaderView(Context context) {
        super(context);
    }

    public ContentDetailsHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ContentDetailsHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
