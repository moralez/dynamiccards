package com.rentpath.dynamiccards.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.rentpath.dynamiccards.model.PdpDetails;
import com.rentpath.dynamiccards.model.ViewProvider;

import java.util.Arrays;
import java.util.List;

/**
 * Created by jmoralez on 4/21/16.
 */
public class ContentRowView extends LinearLayout {

    private List<? extends ViewProvider> mViews;

    public ContentRowView(Context context) {
        super(context);
    }

    public ContentRowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setGravity(Gravity.CENTER);
        setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
        setPadding(15, 15, 15, 15);
    }

    public ContentRowView(Context context, List<? extends ViewProvider> views) {
        this(context, views, 0);
    }

    public ContentRowView(Context context, List<? extends ViewProvider> views, int height) {
        super(context);
        this.mViews = views;
        setGravity(Gravity.CENTER);
        setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                height == 0 ? ViewGroup.LayoutParams.WRAP_CONTENT : height));
        setPadding(15, 15, 15, 15);
    }

    public void setViews(List<ViewProvider> views) {
        this.mViews = views;
        removeAllViews();
        setWeightSum(1);
        for (ViewProvider rowSegmentView : mViews) {
            addView(rowSegmentView.getView(getContext()));
        }
    }

    public ContentRowView getView() {
        removeAllViews();
        setWeightSum(1);
        for (ViewProvider rowSegmentView : mViews) {
            addView(rowSegmentView.getView(getContext()));
        }
        return this;
    }
}
