package com.rentpath.dynamiccards.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.rentpath.dynamiccards.R;

/**
 * Created by jmo on 4/1/2016.
 */
public class ImagePageFragment extends Fragment {

    private ImageView mImageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.layout_image_pager_item, container, false);
        mImageView = (ImageView)rootView.findViewById(R.id.pager_image_view);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle args = getArguments();
        String url = args.getString("imageUrl");
        if (url != null && !url.isEmpty()) {
            Glide.with(this)
                    .load(url)
                    .centerCrop()
                    .into(mImageView);
        }
    }
}

