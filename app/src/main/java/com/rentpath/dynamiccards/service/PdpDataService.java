package com.rentpath.dynamiccards.service;

import com.rentpath.dynamiccards.model.PdpDetails;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by jmo on 4/27/2016.
 */
public interface PdpDataService {
    @GET("show_json/L3VzZXJzLzE1L3Jlc3BvbnNlcy8xMDcv/")
    Call<PdpDetails> getPdpDetails();
}
